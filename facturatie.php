<?php
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set("display_errors", 1);
    

        $projects = fetchAllProject();
        
        
        $file = '/var/www/html/worklog/jiraexport.csv';
        if(is_file($file)){
          unlink($file);
        }

        
        $totalHours = 0;
        $fileRow = "Project; Debiteur;  Uren; prijs; Omschrijving\n";
        file_put_contents($file, $fileRow, FILE_APPEND);


		$i=1;
		

        foreach($projects as $row){

         	if((string)$row->{'Invoicing'} == 'Intern' || (string)$row->{'Invoicing'} == 'Termijn' || (string)$row->{'Invoicing'} == "Termijn PO") continue;
            
          	$jira_project_key       = $row->{'Jira ID'};
         	
         	if(strlen($jira_project_key)<=0) continue;
         	
         	 $afas_project_id        = $row->{"Afas ID"};
             $afas_debiteur_id       = $row->{"Deb. Nr."};
             $project_uur_tarief     = $row->{"Uurprijs"};

             

		
			$res = getDoneIssues($jira_project_key);
	
        
            foreach($res as $jiraKey=>$uurregel){
                

                
                if($project_uur_tarief<=0) continue;

                $fileRow = ($afas_project_id[0]=='0'?"'":"").$afas_project_id.";".($afas_debiteur_id[0]=='0'?"'":"").$afas_debiteur_id.";".str_replace('.',',',$uurregel['hours']).";".$project_uur_tarief.";".$jiraKey." - ".substr($uurregel['description'],0, 93)."\n";

                file_put_contents($file, $fileRow, FILE_APPEND);
                $fileRow = '';
                
                $totalHours+=(float)$uurregel['hours'];
                
            }
            
        }
            
        
        echo "TotalHours: ".$totalHours."<br>";
        echo '<a href="worklog/jiraexport.csv">Download '.$filedate.'</a>';
        die;
	
	
	
	
	
	
	function getDoneIssues($project_key = ''){
    	
        $username = 'gijs.epping';
        $password = '07Epping';
        
        $dStart = substr($_GET['startdate'], 0,2);
        $mStart = substr($_GET['startdate'], 3,2);
        $jStart = substr($_GET['startdate'], 6,2);
        
        
        $dEind = substr($_GET['enddate'], 0,2);
        $mEind = substr($_GET['enddate'], 3,2);
        $jEind = substr($_GET['enddate'], 6,2);
 
        
        $startdate = date('Y-m-d%2000:00', strtotime($jStart.'-'.$mStart.'-'.$dStart));
        $enddate = date('Y-m-d%2023:59', strtotime($jEind.'-'.$mEind.'-'.$dEind));
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);    	
    	
    	
         $url = 'https://shopworks.atlassian.net/rest/api/latest/search?jql=project%20%3D%20'.$project_key.'%20AND%20updated>%3D"'.$startdate.'"%20AND%20updated%20<%3D"'.$enddate.'"AND%20status%20in%20(done)%20AND%20issuetype%20not%20in%20subtaskIssueTypes()&format=xml&maxResults=500';
        
        
    	
    	curl_setopt($curl, CURLOPT_URL, $url);



        $issues = curl_exec($curl);

        
        $array = json_decode($issues,TRUE);

        $return = array();
        $totalHours = 0;
        if(sizeof($array['issues'])>0){
            foreach($array['issues'] as $issues){

                
                $kHours = getTempoHours($issues['key']);
                
                
                if($kHours>0){
	                $return[$issues['key']]['hours'] = $kHours;
	                $return[$issues['key']]['description'] = $issues['fields']['summary'];
	                $totalHours += $kHours;
                }
                
                
            }
            
            if($totalHours>0){
                $return['PM']['hours'] = ($totalHours*0.15);
                $return['PM']['description'] = 'CP/PM 15%';
            }
        }
        
        return $return;


	}
	
	
	
function getTempoHours($key){
    
    $startdate = date('Y-m-d', strtotime('2016-01-01'));
    $enddate = date('Y-m-d');

    
    $selfUrl = 'https://app.tempo.io/api/1/getWorklog?baseUrl=https://shopworks.atlassian.net&issueKey='.$key.'&dateFrom='.$startdate.'&dateTo='.$enddate.'&diffOnly=false&tempoApiToken=6fa415f7-f388-458b-bb68-3f49b40d20ee&format=xml';
     

    $username = 'gijs.epping';
    $password = '07Epping';
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($curl, CURLOPT_URL, $selfUrl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

    $issue_list = (curl_exec($curl));
    $json = $xml = "";
    $xml = simplexml_load_string($issue_list);
    $json = json_encode($xml);
    $array = json_decode($json,TRUE);        


    
    $hours = 0;

    
    if($array['@attributes']['number_of_worklogs'] == 1){
            
        return (float)$array['worklog']['hours'];

        
    }elseif($array['@attributes']['number_of_worklogs'] > 1) {
        foreach($array['worklog'] as $key => $workLog){

            $hours += $workLog['hours'];
        }
        
        return $hours;
    }
}
	
	
    
function fetchAllProject() {


	$autherization = "Authorization: Basic M2hFcmZ0OS9hOGdad3V4QjZjNHFIMkRUSWxtQWcwVmo5ODFWSjNtMHpoOTIwdDE4N2JJQkhUMWFVVitOaktIcncvTmk5S1pSbmJNPQ==";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
	    $autherization
	]);

   
	// Set query data here with the URL
	curl_setopt($ch, CURLOPT_URL, 'https://eu2.ragic.com/shopworks/project-management/1'); 
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 3);
	$content = trim(curl_exec($ch));
  
	curl_close($ch);
   
   
	$res =  json_decode($content);
   
   
   return $res;
}
	  