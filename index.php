<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');



	$jsonResult = array();
	$aanbieders = getMoney('/aanbieders?json={"productsoorten":[271]}'); // aanbieders
	$i=0;
	foreach($aanbieders as $aanbieder){
		

		$products = getMoney('/productkenmerken?json={"data":true,"details":true,"MPR":false,"productsoorten":[271],"aanbieders":['.$aanbieder->id.'],"producten":[24691,13472,17671,22335],"tabelkenmerken":[34178]}');

		$jsonResult[$i]['id'] = $i;
		$jsonResult[$i]['name'] = $aanbieder->naam;
		$jsonResult[$i]['logo'] = str_replace(' ','-',$aanbieder->naam).'.jpg';
		$jsonResult[$i]['interest'] = (float)$products[0]->data;
		$jsonResult[$i]['monthlyexpenses'] = calPMT($products[0]->data, 24, 10000);
		$jsonResult[$i]['link'] = 'https://aanvragen.kredietdesk.nl/?utm_source=marktplaats';
		$jsonResult[$i]['info']['title'] = 'Deze kredits '.$products[0]->details;
		$jsonResult[$i]['info']['list'] = '<ul><li>De mogelijkheid tot kredietverstrekking kan op voorhand niet bepaald worden.</li><li>BKR toetsing maakt onderdeel uit van de beoordeling.</li><li>Bij een A-codering wordt alleen een krediet verstrekt indien er inmiddels sprake is van een H-codering.</li></ul>';

				
		$i++;
	}
	
	echo json_encode($jsonResult);
	
	//getMoney('/tabelkenmerken?json={"productsoorten":[271]}'); // aanbieders
	//getMoney('/producten?json={"productsoorten":[271]}'); // producten per bank
	//getMoney('/productkenmerken?json={"data":true,"details":true,"MPR":false,"productsoorten":[271],"aanbieders":[1547,1608,1717,2126],"producten":[24691,13472,17671,22335],"tabelkenmerken":[34178]}');
	
	
	
function calPMT($apr, $term, $loan)
{
  $term = $term * 12;
  $apr = $apr / 1200;
  $amount = $apr * -$loan * pow((1 + $apr), $term) / (1 - pow((1 + $apr), $term));
  return round($amount,2);
}

	
	
function getMoney($url){
	
	$user = "user: Marktpl@@ts";
	$pass = "pwd: Ws3Rt21!";
	$organisationcode = "organisationcode: MPL";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
	    $user,
			$pass,
			$organisationcode
	]);
   
	// Set query data here with the URL
	curl_setopt($ch, CURLOPT_URL, 'https://api.moneyview.nl'.$url); 
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 3);
	$content = trim(curl_exec($ch));
  
	//echo "<pre>";print_r(json_decode($content));
	
	curl_close($ch);
	
	return json_decode($content);

}
?>