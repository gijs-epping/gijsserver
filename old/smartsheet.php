<?php
    error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);

$data = array('key' => "VAL-12", 'time' => "0.5",'date' => "12-02-17", 'user' => "Gijs Epping");


$jsonEncodedData = json_encode($data);

    $curl = curl_init();

$opts = array(
    CURLOPT_URL             => 'https://hooks.zapier.com/hooks/catch/1084296/m9y8s7/',
    CURLOPT_RETURNTRANSFER  => true,
    CURLOPT_CUSTOMREQUEST   => 'POST',
    CURLOPT_POST            => 1,
    CURLOPT_POSTFIELDS      => $jsonEncodedData,
    CURLOPT_HTTPHEADER      => array('Content-Type: application/json','Content-Length: ' . strlen($jsonEncodedData))                                                                       
);


    // Set curl options
    curl_setopt_array($curl, $opts);

    // Get the results
    $result = curl_exec($curl);

    // Close resource
    curl_close($curl);

    echo $result;

?>