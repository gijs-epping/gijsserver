<?php
/**
* Get these from https://account.live.com/developers/applications
*/
define("LC_CLIENT_ID", "000000004015E878");
define("LC_CLIENT_SECRET", "3qFwg-cZ5x3Q8GfAP7gROira35-ld2pL");

// this should match the URL you gave when getting your CLIENT_SECRET
define("LC_CALLBACK_URL", "http://www.5500cc.com/onenote-dev/callback.php");

require_once __DIR__ . '/vendor/autoload.php';

use Siftware\LiveConnect;
use Siftware\Logger;

/**
* PSR-3 compatible logger. Logs to file,  if you want to disable logging then just
* pass false as second parameter. See the class interface for more options.
* You can of course ditch this and pass in your own PS3-R logger instance
*/


$logger = new Logger(Psr\Log\LogLevel::DEBUG);

$liveConnect = new LiveConnect(LC_CLIENT_ID, LC_CLIENT_SECRET, LC_CALLBACK_URL, $logger);

/**
* See here for a full list of scopes and what they are used for:
* http://msdn.microsoft.com/en-us/library/live/hh243646.aspx
*/
$liveConnect->setScopes("wl.offline_access, wl.signin, wl.basic, office.onenote_update");
