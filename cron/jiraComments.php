<?php
ini_set("display_errors", 1);
error_reporting(E_ALL & ~E_NOTICE);




$hostname = "localhost";
$username = "root";
$password = "07Epping";
$dbname = 'jira';
$dbh =null;
try {
    $dbh = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
}
catch(PDOException $e)
{
   echo $e->getMessage();
}




// FIRST TIME CHECKED
$checkdate = (time()-(60*60*24)); //ouder dan 24 uur;
$q = $dbh->query("SELECT * FROM comments WHERE reactiondate = 0 AND insertdate<=".$checkdate." AND timeschecked=0 ORDER BY insertdate DESC");
while($row = $q->fetch ()){


  if (!preg_match("/shopworks/i", $row['username'])) {
   notifyOverdue('24 uur overdue', 'Er is 1 dag geen reactie geweest op deze story', 'https://shopworks.atlassian.net/browse/'.$row['storykey'], '#33cc33', 'true');
   $dbh->query("UPDATE comments SET timeschecked=1 WHERE id = ".$row['id']."");
 }

}


// SECOND TIME CHECKED
$checkdate = (time()-(60*60*48)); //ouder dan 36 uur;
$q = $dbh->query("SELECT * FROM comments WHERE reactiondate = 0 AND insertdate<=".$checkdate." AND timeschecked=1 ORDER BY insertdate DESC");
while($row = $q->fetch()){

  if (!preg_match("/shopworks/i", $row['username'])) {
    notifyOverdue('2 dagen overdue', 'Er is 2 dagen geen reactie geweest op deze story', 'https://shopworks.atlassian.net/browse/'.$row['storykey'], '#ffff00', 'true');
    $dbh->query("UPDATE comments SET timeschecked=2 WHERE id = ".$row['id']."");
  }

}


// THIRD TIME CHECKED
$checkdate =  (time()-(60*60*72)); //ouder dan 72 uur;
$q = $dbh->query("SELECT * FROM comments WHERE reactiondate = 0 AND insertdate<=".$checkdate." AND timeschecked=2 ORDER BY insertdate DESC");
while($row = $q->fetch()){

  if (!preg_match("/shopworks/i", $row['username'])) {
      notifyOverdue('ALERT 3 dagen overdue', 'ALERT!! Er is 3 dagen geen reactie geweest op story', 'https://shopworks.atlassian.net/browse/'.$row['storykey'], '#ff0000', 'true');
      $dbh->query("UPDATE comments SET timeschecked=3 WHERE id = ".$row['id']."");
  }

}

// CUSTOMER LATE RESPONSE CHECKS

/*
// FIRST TIME CHECKED CUSTOMER LATE RESPONSE CHECKS
$checkdate =  (time()-(60*60*72)); //ouder dan 72 uur;
$q = mysql_query("SELECT * FROM comments WHERE reactiondate = 0 AND insertdate<=".$checkdate." AND timeschecked=0 ORDER BY insertdate DESC");
while($row = mysql_fetch_assoc($q)){

  if (preg_match("/shopworks/i", $row['username'])) {
      notifyOverdue('De klant heeft al 3 dagen niet gereageerd op de story: https://shopworks.atlassian.net/browse/'.$row['storykey'], 'yellow', 'true');
      mysql_query("UPDATE comments SET timeschecked=1 WHERE id = ".$row['id']."");
  }

}


// SECOND TIME CHECKED CUSTOMER LATE RESPONSE CHECKS
$checkdate =  (time()-(60*60*120)); //ouder dan 5 dagen
$q = mysql_query("SELECT * FROM comments WHERE reactiondate = 0 AND insertdate<=".$checkdate." AND timeschecked=0 ORDER BY insertdate DESC");
while($row = mysql_fetch_assoc($q)){

  if (preg_match("/shopworks/i", $row['username'])) {
      notifyOverdue('De klant heeft al 5 dagen niet gereageerd op de story: https://shopworks.atlassian.net/browse/'.$row['storykey'], 'red', 'true');
      mysql_query("UPDATE comments SET timeschecked=2 WHERE id = ".$row['id']."");
  }

}
*/


function notifyOverdue($titel,$message, $link, $color = '#ffff00', $notify = 'false'){


  $data = array(
                "text"=>"Reaction overdue",
                "color" => $color,
                "attachments" => array([
                        "title"=> $titel,
                        "title_link" => $link,
                        "text"=>$message."\n". $link
                      ])
              );

  $data_string = json_encode($data);


  $url = "https://hooks.slack.com/services/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";
  $c = curl_init();
  curl_setopt($c, CURLOPT_URL, $url);
  curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($c, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($c, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($c, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data_string))
  );

  curl_exec($c);
}

 ?>
