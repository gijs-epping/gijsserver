<?php
error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);


$projectKeys = fetchAllProject();

getWorklogs($projectKeys);





function getWorklogs($projectKeys = ''){
    
	$medewerkers = array();
	$medewerkers['suzan.fokker@shopworks.nl'] 			= 'S037';
	$medewerkers['leoni.tielrooy@shopworks.nl'] 		= 'S029';
	$medewerkers['jos.de.hoon@shopworks.nl'] 				= 'S028';
	$medewerkers['tim.breedveld@shopworks.nl'] 			= 'S023';
	$medewerkers['fatih.celik@shopworks.nl'] 				= 'S038';
	$medewerkers['richard.van.yperen@shopworks.nl'] 			= 'S034';
	$medewerkers['casper.paulich@shopworks.nl'] 					= 'S042';
	$medewerkers['alexander.wttewaall@shopworks.nl'] 			= 'S039';
	$medewerkers['martijn.snijder@shopworks.nl']	 				= 'S033';
	$medewerkers['ron.peeters@shopworks.nl'] 							= 'S021';
	$medewerkers['jimmy.de.graaf@shopworks.nl'] 					= 'S020';
	$medewerkers['andre.kuijer@shopworks.nl'] 						= 'S041';
	$medewerkers['fleur.huisman@shopworks.nl'] 						= 'S036';
	$medewerkers['gijs.epping@shopworks.nl'] 							= 'S002';
	$medewerkers['elise.van.eekhout@shopworks.nl'] 				= 'S043';
	$medewerkers['tomas@shopworks.nl'] 							= 'S001';
	$medewerkers['tomas.hesseling@shopworks.nl'] 					= 'S001';
	$medewerkers['sander.de.veen@shopworks.nl'] 					= 'S044';
	$medewerkers['tim.vroom@supportdesk.nu'] 							= 'S002';
	$medewerkers['carst.vander.molen@shopworks.nl'] 			= 'S047';
	$medewerkers['elisa.knauf@shopworks.nl'] 							= 'S045';
	$medewerkers['pauline.wardenier@shopworks.nl'] 				= 'S046';
	$medewerkers['maaike.verhoeven@shopworks.nl']					= 'S048';
	$medewerkers['rehsmi.eikhout@shopworks.nl']						= 'S049';
	$medewerkers['jessie.stam@shopworks.nl']							= 'S002';
	$medewerkers['mees.frenkel@shopworks.nl']							= 'S052';
	
	//$medewerkers['michiel'] 						= 'S009';
	//$medewerkers['maarten.jongerius'] 						= 'S011';
	//$medewerkers['joshua.van.londen'] 						= 'S040';
	//$medewerkers['xander'] 												= 'S005';
	//$medewerkers['robert'] 												= 'S002';
	//$medewerkers['danny.wijffelaars'] 					= 'S002';
	//$medewerkers['floris.veenhuis'] 						= 'S031';
	//$medewerkers['epuy'] 													= 'S002';
//	$medewerkers['ali.qsadiq'] 									= 'S002';
//$medewerkers['tran.vo']												= 'S002';
		


	
    if(!$_GET['startdate']){
        echo "geen startdatum opgegeven";    
    }
    
   
    
    $dStart = substr($_GET['startdate'], 0,2);
    $mStart = substr($_GET['startdate'], 3,2);
    $jStart = substr($_GET['startdate'], 6,2);
    
    
    $dEind = substr($_GET['enddate'], 0,2);
    $mEind = substr($_GET['enddate'], 3,2);
    $jEind = substr($_GET['enddate'], 6,2);
    
    $filedate = date('Y-m', strtotime($jStart.'-'.$mStart.'-'.$dStart));
		
		
    $startdate = date('Y-m-d', strtotime($jStart.'-'.$mStart.'-'.$dStart));
    $enddate = date('Y-m-d', strtotime($jEind.'-'.$mEind.'-'.$dEind));

	 $selfUrl = 'https://app.tempo.io/api/1/getWorklog?dateFrom='.$startdate.'&dateTo='.$enddate.'&diffOnly=false&addUserDetails=true&tempoApiToken=6fa415f7-f388-458b-bb68-3f49b40d20ee&baseUrl=https://shopworks.atlassian.net&format=xml';
	
   
   
     

    $username = 'gijs.epping';
    $password = '07Epping';
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($curl, CURLOPT_URL, $selfUrl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

    $issue_list = (curl_exec($curl));




    $array = $xml = new SimpleXMLElement($issue_list);        
	
	//	echo "<pre>";print_r($array);
    
    $file = '/var/www/html/worklog/productiviteit-'.$filedate.'.csv';
    if(is_file($file)){
      unlink($file);
    }
    
    $timeArray = array();
    $totalHours = 0;
    
    $row = "Project ID;Medewerker ID;Medewerker naam;Gewerkt h;datum;issue;projectname;\n";    
    file_put_contents($file, $row, FILE_APPEND);
    
    foreach($array->worklog as $worklog){
         $projectkey = explode('-',$worklog->issue_key)[0];
         if($projectKeys[$projectkey]){
             
             $prKey 		= $projectKeys[$projectkey][0];
             $prName 		= $projectKeys[$projectkey][1];
             $type 			= $projectKeys[$projectkey][2];
						 $ragicid 	= $projectKeys[$projectkey][3];

             if (preg_match("/ISV/i", $worklog->issue_key)) continue;
             
	
             if($medewerkers[(string)$worklog->user_details->email]){
	             $medewerker = $medewerkers[(string)$worklog->user_details->email];
	          }else{
					echo (string)$worklog->user_details->email." is niet gekoppeld<br>";
				}

				$date = date('d/m/Y', strtotime($worklog->work_date));
	            $row = ($prKey[0]=='0'?"'":"").$prKey.";".$medewerker.";".$worklog->user_details->email.";".str_replace(".",",",$worklog->hours).";".$worklog->work_date.";".$worklog->issue_key.";	".$prName.";\n";
	     
	             
	            file_put_contents($file, $row, FILE_APPEND);
	            $row = '';	             
	            
	            $totalHours+=(float)$worklog->hours;
             

         }else{
             echo "Project niet gevonden in Ragic ".$projectkey." - ".$worklog->issue_key." <a target=\"_blank\" href=\"https://eu2.ragic.com/shopworks/project-management/1#!/1/".$ragicid."\">Pas hier aan</a> <br>";
         }
        
    }



    echo "<b>TotalHours:</b> ".$totalHours."<br>";
    echo '<b>Download:</b> <a href="worklog/productiviteit-'.$filedate.'.csv"> '.$filedate.'</a>';
    die;

  }

          

function fetchAllProject() {


	$autherization = "Authorization: Basic M2hFcmZ0OS9hOGdad3V4QjZjNHFIMkRUSWxtQWcwVmo5ODFWSjNtMHpoOTIwdDE4N2JJQkhUMWFVVitOaktIcncvTmk5S1pSbmJNPQ==";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
	    $autherization
	]);

   
	// Set query data here with the URL
	curl_setopt($ch, CURLOPT_URL, 'https://eu2.ragic.com/shopworks/project-management/1'); 
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 3);
	$content = trim(curl_exec($ch));
  
	curl_close($ch);
   
   
	$projects =  json_decode($content);
   
	$error = 0;
	
	//echo "<pre>"; print_r($projects);die;
	 
	$projectKeys = array();
	foreach($projects as $row){
			
			$afas_project_id        = $row->{"Afas ID"};
			$afas_project_name      = $row->{"Projectname"};
			$jira_project_key       = $row->{"Jira ID"};
			$type      			 	= $row->{"Invoicing"};
			$ragicID        		= $row->{"_ragicId"};

			if($afas_project_id == ""){
					$error = 1;
					echo $row->{"Type project"}."-".$row->{"Projectname"}." Heeft geen afas id, pas dit <a target=\"_blank\" href=\"https://eu2.ragic.com/shopworks/project-management/1#!/1/".$row->{"_ragicId"}."\">HIER</a> aan<br>";
			}
			
			$projectKeys[strtoupper($jira_project_key)] = array($afas_project_id, $afas_project_name, $type, $ragicID);
			
	}
	
	if($error == 1) echo ("<h1>Errors found</h1>You can fix them in Excel or remove them, sometimes they are sales hours.<br><br>");
	
   return $projectKeys;
}
          
          
          
