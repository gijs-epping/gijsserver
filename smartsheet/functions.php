<?php
	
	
function updateObject($id, $name, $type = 'sheets'){
	global $projectName;
	
	$sheetURL = "https://api.smartsheet.com/2.0/".$type."/".$id;
	$inputToken = '5svxntezbf3dic7sy8sqsw8mtm';	
	

	$headers = array(
		"Authorization: Bearer " .$inputToken,
		'Content-Type: application/json'      
		);

		
	$data = array(
			"name" => str_replace("{project}", $projectName, $name)
		);
	
	$data_string = json_encode($data);   
		
	// Create Headers Array for Curl
	$headers = array(
		"Authorization: Bearer " .$inputToken,
		'Content-Type: application/json',
		'Content-Length: ' . strlen($data_string)
	);
	
				// Connect to Smartsheet API to get Sheet List
	$curlSession = curl_init($sheetURL);
	curl_setopt($curlSession, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curlSession, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curlSession, CURLOPT_POSTFIELDS,$data_string);
	         	
	         	
	$response = curl_exec($curlSession);
    if(!$response) {
        return false;
    }		         	 
	
}





function getPlaceholder($id, $type = 'workspaces'){
	
	$sheetURL = "https://api.smartsheet.com/2.0/".$type."/".$id;
	$inputToken = '5svxntezbf3dic7sy8sqsw8mtm';		
	
			// Create Headers Array for Curl
		$headers = array(
			"Authorization: Bearer " .$inputToken,
			'Content-Type: application/json'      
			);
		
			// Connect to Smartsheet API to get Sheet List
		$curlSession = curl_init($sheetURL);
		curl_setopt($curlSession, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, TRUE);
		

		$smartsheetData = curl_exec($curlSession);
		// Assign response to PHP object
		$sheetsObj = json_decode($smartsheetData);
	

		foreach($sheetsObj->sheets as $sheet){
			updateObject($sheet->id, $sheet->name,"sheets");
		}
		
		
		foreach($sheetsObj->folders as $placeholder){
			getPlaceholder($placeholder->id, "folders");
		}
		
		foreach($sheetsObj->sights as $sight){
			updateObject($sight->id, $sight->name, "sights");
		}
		

}	




function updateFolder($id){
	
	
}