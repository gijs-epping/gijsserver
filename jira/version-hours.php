<?php
error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);



if(!$_GET['projectID'])
  {
      echo "search projectID";
      die;
  }

  $result = fetchProject($_GET['projectID']);
//    echo "<pre>";  print_r($result);  die;
?>

<!DOCTYPE HTML>
<html>

<head>
  <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Overzicht uren gepland t.o.v. gewerkt"
      },
      animationEnabled: false,
      legend: {
        verticalAlign: "bottom"
      },
      data: [

      {
        type: "bar",
        showInLegend: true,
        legendText: "Gepland",
        dataPoints: [
          <?php
            foreach($result as $key=>$version){
           ?>
        {  y: <?php echo ($version['totals']['total']['totaltime-estimate']?$version['totals']['total']['totaltime-estimate']:0);?>,  label: "<?php echo $version['name'];?>" },

        <?php
          }
         ?>


        ]
      },
      {
        type: "bar",
        showInLegend: true,
        legendText: "Gewerkt",
        dataPoints: [
          <?php

            foreach($result as $key=>$version){
           ?>
            { y: <?php echo ($version['totals']['total']['totaltime-worked']?$version['totals']['total']['totaltime-worked']:0);?>,  label: "<?php echo $version['name'];?>" },

        <?php
        }
         ?>


        ]
      }

      ]
    });

chart.render();
}
</script>
<script type="text/javascript" src="canvasjs.min.js"></script></head>
<body>
  <div id="chartContainer" style="height: 400px; width: 100%;">
  </div>
</body>

</html>





<?php

// http://pastebin.com/gzvnkHbC


function fetchProject($projectID){

    $username = 'gijs.epping';
    $password = '07Epping';


    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);




    $totalArray = array();

    $versions = fetch($curl, 'https://shopworks.atlassian.net/rest/api/2/project/'.$projectID.'/versions');


    foreach($versions as $version){


        if($version->archived == 1 && $version->released == 1) continue;
        $totalArray['version'][$version->id]['name'] = $version->name;
        $totalArray['version'][$version->id]['totals'] = checkAmountStories($projectID, $version->id, $curl);

    }

    return $totalArray['version'];
}



function checkAmountStories($projectID, $fixedVersion,$curl){

  $data = array("jql" => "project = '".$projectID."' AND fixVersion = ".$fixedVersion."", 'maxResults'=>'150');

  $issues = fetch($curl, 'https://shopworks.atlassian.net/rest/api/2/search?'.http_build_query($data));


  $storyAmountDiscipline = array();

  foreach($issues->issues as $issue){



        if($issue->fields->issuetype->name == 'Epic') continue;


        $storyAmount['total']['totalstories'] += 1;
        $storyAmount['total']['totaltime-estimate'] += (integer)($issue->fields->aggregatetimeoriginalestimate/60/60);

        if($issue->fields->status->statusCategory->key == 'done'){
            $storyAmount['work']['timeworked-done'] += (integer)($issue->fields->aggregatetimespent/60/60);
        }else{
          $storyAmount['work']['timeworked-worked'] += (integer)($issue->fields->aggregatetimespent/60/60);
        }

        $storyAmount['total']['totaltime-worked'] += (float)($issue->fields->aggregatetimespent/60/60);

  }


  return $storyAmount;
}

function fetch($curl, $selfUrl){


  curl_setopt($curl, CURLOPT_URL, $selfUrl);

  $issue_list = (curl_exec($curl));
  $res = json_decode($issue_list);
  return $res;
}
 ?>
