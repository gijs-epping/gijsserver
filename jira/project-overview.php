<?php

error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);


$project = fetchProject($_GET['projectID']);


$stories = checkAmountStories($_GET['projectID']);

?>


<!DOCTYPE HTML>
<html>

<head>
  <title><?php echo $project->name;?></title>
  <style>
  body {
   font-family: Arial, Helvetica, sans-serif;
   font-size: 13px;
  }
  </style>
</head>

<body>
<h1><?php echo $project->name;?> - Week <?php echo date('W');?></h1>
<h4>Dit project overzicht is gegenereerd op <?php echo date('d/M/Y H:i');?></h4>

<iframe width="800px;" height="450px;" src="version-hours.php?projectID=<?php echo $_GET['projectID']; ?>" frameborder="0"></iframe>


<h2>Totalen van project</h2>
<table>
  <tr><td>Totaal stories:</td><td><?php echo $stories['total']['totalstories'];?></td></tr>
  <tr><td>Totaal ingeschat:</td><td><?php echo $stories['total']['totaltime-estimate'];?></td></tr>
  <tr><td>Totaal gewerkt:</td><td><?php echo $stories['total']['totaltime-worked'];?></td></tr>
  </tr>
</table>

</body>

</html>



<?php



/*

Array
(
    [total] => Array
        (
            [totalstories] => 36
            [totaltime-estimate] => 299
            [totaltime-worked] => 205
        )

    [work] => Array
        (
            [timeworked-worked] => 127
            [timeworked-done] => 70
        )

)*/

// http://pastebin.com/gzvnkHbC




function fetchProject($projectID){

    $username = 'gijs.epping';
    $password = '07Epping';


    $curl = curl_init();
    curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);




    $totalArray = array();

    $project = fetch($curl, 'https://shopworks.atlassian.net/rest/api/2/project/'.$projectID);

    return $project;
}



function checkAmountStories($projectID){


  $username = 'gijs.epping';
  $password = '07Epping';


  $curl = curl_init();
  curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);


  $data = array("jql" => "project = '".$projectID."'", 'maxResults'=>'150');



  $issues = fetch($curl, 'https://shopworks.atlassian.net/rest/api/2/search?'.http_build_query($data));


  $storyAmountDiscipline = array();


  foreach($issues->issues as $issue){



        if($issue->fields->issuetype->name == 'Epic') continue;


        $storyAmount['total']['totalstories'] += 1;
        $storyAmount['total']['totaltime-estimate'] += (integer)($issue->fields->aggregatetimeoriginalestimate/60/60);

        if($issue->fields->status->statusCategory->key == 'done'){
            $storyAmount['work']['timeworked-done'] += (integer)($issue->fields->aggregatetimespent/60/60);
        }else{
          $storyAmount['work']['timeworked-worked'] += (integer)($issue->fields->aggregatetimespent/60/60);
        }

        $storyAmount['total']['totaltime-worked'] += (float)($issue->fields->aggregatetimespent/60/60);

  }


  return $storyAmount;
}





function fetch($curl, $selfUrl){

  curl_setopt($curl, CURLOPT_URL, $selfUrl);

  $issue_list = (curl_exec($curl));
  $res = json_decode($issue_list);
  return $res;
}
